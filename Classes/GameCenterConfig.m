//
//  GameCenterConfig.m
//  All Game
//
//  Created by o0402 on 11-5-19.
//  Copyright 2011 xiaoxin All rights reserved.
//

#import "GameCenterConfig.h"
#import "GameDataDefined.h"

@interface GameCenterConfig(PrivateMethod)  // 1.Private 2.PrivateMethod 3.PrivateMethods
@end

static GameCenterConfig* instance = nil; 

@implementation GameCenterConfig

+ (id)shareGameCenterInstance
{
	if (nil == instance) 
	{
		GameCenterConfig* shareInstance = [[GameCenterConfig alloc] init];
		instance = shareInstance;
	}
	return instance;
}

- (id)init
{
	if ((self = [super init]))
	{
		m_pViewController = nil;
		[self initGameCenter];
	}
	return self;
}

- (void)initGameCenter 
{
	if ([self isGameCenterAvailable]) 
	{
		[self authenticateLocalPlayer];
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		[nc addObserver:self 
			selector:@selector(gameCenterAuthenticationChanged) 
			name:GKPlayerAuthenticationDidChangeNotificationName 
			object:nil];
	}
}

- (void)initGameCenterWithViewController:(UIViewController*)viewController
{
	m_pViewController = viewController;
	return [self initGameCenter];
}

- (BOOL)isGameCenterAvailable
{
	Class gcClass = (NSClassFromString(@"GKLocalPlayer"));
	NSString *reqSysVer = @"4.1";
	NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
	BOOL osVersionSupported = ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending);
	return (gcClass && osVersionSupported);
}

- (void)authenticateLocalPlayer
{
	[[GKLocalPlayer localPlayer] authenticateWithCompletionHandler:^(NSError *error)
	 {
		 if (error == nil) 
		 {
			 //成功处理
			 CCLOG(@"成功");
			 CCLOG(@"1--alias--.%@",[GKLocalPlayer localPlayer].alias);
			 CCLOG(@"2--authenticated--.%d",[GKLocalPlayer localPlayer].authenticated);
			 CCLOG(@"3--isFriend--.%d",[GKLocalPlayer localPlayer].isFriend);
			 CCLOG(@"4--playerID--.%@",[GKLocalPlayer localPlayer].playerID);
			 CCLOG(@"5--underage--.%d",[GKLocalPlayer localPlayer].underage);
			 
			 //[self reportScore:34 forCategory:@"STAGE1"];
			 //[self retrieveTopTenScores:@"STAGE1"];
		 }
		 else
		 {
			 //错误处理
			 CCLOG(@"失败  %@",error);
		 }
	 }];
}

- (void)gameCenterAuthenticationChanged 
{
	if ([GKLocalPlayer localPlayer].isAuthenticated)
	{
		// Insert code here to handle a successful authentication.
	}
	else
	{
		// Insert code here to clean up any outstanding Game Center-related classes.
	}
}

- (void)reportScore:(int64_t)score forCategory:(NSString*)category
{
	GKScore *scoreReporter = [[[GKScore alloc] initWithCategory:category] autorelease];
	scoreReporter.value = score;
	[scoreReporter reportScoreWithCompletionHandler:^(NSError *error) 
	{
		if (error != nil)
		{
			CCLOG(@"上传分数出错");
			// 当上传分数出错的时候,要将上传的分数存储起来,比如将SKScore存入一个NSArray中.等可以上传的时候再次尝试.
		}
		else 
		{
			CCLOG(@"上传分数成功");
		}

	}];
}

// 响应用户显示排行榜的事件
- (void)showLeaderboard
{
	return [self showLeaderboardFromViewController:m_pViewController];
}

// 响应用户显示排行榜的事件
- (void)showLeaderboardFromViewController:(UIViewController*)viewController
{
	m_pViewController = viewController;
	
	GKLeaderboardViewController *leaderboardController = [[[GKLeaderboardViewController alloc] init] autorelease];
	if (nil != leaderboardController) 
	{
		leaderboardController.leaderboardDelegate = self;
		if (isPad)
		{
			//leaderboardController.timeScope = GKLeaderboardTimeScopeToday;
			//leaderboardController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
			//leaderboardController.modalPresentationStyle = UIModalPresentationFormSheet;
		}
		[viewController presentModalViewController: leaderboardController animated: YES];
	}	
}

// 响应用户关闭排行榜的事件
-(void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	[m_pViewController dismissModalViewControllerAnimated:YES];
}

// 显示成就
- (void)showAchievement
{
	return [self showAchievementFromViewController:m_pViewController];
}

// 显示成就
- (void)showAchievementFromViewController:(UIViewController*)viewController
{
	m_pViewController = viewController;
	GKAchievementViewController* achievementController = [[[GKAchievementViewController alloc] init] autorelease];
	if (nil != achievementController) 
	{
		achievementController.achievementDelegate = self;
		[viewController presentModalViewController: achievementController animated: YES];
	}
}

// 响应用户关闭成就的事件
- (void)achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
	[m_pViewController dismissModalViewControllerAnimated:YES];
}

// 说明:
// 1)    playerScope:表示检索玩家分数范围.
// 2)    timeScope:表示某一段时间内的分数
// 3)    range:表示分数排名的范围
// 4)    category:表示你的Leaderboard的ID.
- (void)retrieveTopTenScores:(NSString*)category
{
	GKLeaderboard *leaderboardRequest = [[GKLeaderboard alloc] init];
	if (leaderboardRequest != nil)
	{
		leaderboardRequest.playerScope = GKLeaderboardPlayerScopeGlobal;
		leaderboardRequest.timeScope = GKLeaderboardTimeScopeAllTime;
		leaderboardRequest.range = NSMakeRange(1,10);
		leaderboardRequest.category = category;
		[leaderboardRequest loadScoresWithCompletionHandler: ^(NSArray *scores, NSError *error)
		{
			if (error != nil)
			{
				CCLOG(@"下载失败");
			}
			if (scores != nil)
			{
				CCLOG(@"下载成功....");
				NSArray *tempScore = [NSArray arrayWithArray:leaderboardRequest.scores];
				 for (GKScore *obj in tempScore) 
				 {
					 CCLOG(@"    playerID            : %@",obj.playerID);
					 CCLOG(@"    category            : %@",obj.category);
					 CCLOG(@"    date                : %@",obj.date);
					 CCLOG(@"    formattedValue		 : %@",obj.formattedValue);
					 CCLOG(@"    value               : %d",obj.value);
					 CCLOG(@"    rank                : %d",obj.rank);
					 CCLOG(@"**************************************");
				 }
			}
		}];
	}
}

- (void)retrieveFriends
{
	GKLocalPlayer *lp = [GKLocalPlayer localPlayer];
	if (lp.authenticated)
	{
		[lp loadFriendsWithCompletionHandler:^(NSArray *friends, NSError *error) 
		{
			if (error == nil)
			{
				[self loadPlayerData:friends];
			}
			else 
			{
				// report an error to the user.	
			}

		}];
	}
}

- (void)loadPlayerData:(NSArray*)identifiers
{
	[GKPlayer loadPlayersForIdentifiers:identifiers withCompletionHandler:^(NSArray *players, NSError *error) 
	{
		if (error != nil)
		{
			// handle an error.
		}
		if (players != nil)
		{
			CCLOG(@"得到好友的alias成功");
			GKPlayer *friend1 = [players objectAtIndex:0];
			CCLOG(@"friedns---alias---%@",friend1.alias);
			CCLOG(@"friedns---isFriend---%d",friend1.isFriend);
			CCLOG(@"friedns---playerID---%@",friend1.playerID);
		}
	}];
}

// 函数的参数中identifier是你成就的ID, percent是该成就完成的百分比
- (void) reportAchievementIdentifier:(NSString*)identifier percentComplete:(float)percent
{
	GKAchievement *achievement = [[[GKAchievement alloc] initWithIdentifier: identifier] autorelease];
	if (achievement)
	{
		achievement.percentComplete = percent;
		[achievement reportAchievementWithCompletionHandler:^(NSError *error)
		{
			if (error != nil)
			{
				CCLOG(@"报告成就进度失败 ,错误信息为: \n %@",error);
			}
			else 
			{
				CCLOG(@"报告成就进度---->成功!");	
				CCLOG(@"    completed:%d",achievement.completed);
				CCLOG(@"    hidden:%d",achievement.hidden);
				CCLOG(@"    lastReportedDate:%@",achievement.lastReportedDate);
				CCLOG(@"    percentComplete:%f",achievement.percentComplete);
				CCLOG(@"    identifier:%@",achievement.identifier);
			}

		}];
	}
}

- (void)loadAchievements
{
	NSMutableDictionary *achievementDictionary = [[NSMutableDictionary alloc] init];
	[GKAchievement loadAchievementsWithCompletionHandler:^(NSArray *achievements,NSError *error)
	{
		if (error == nil) 
		{
			NSArray *tempArray = [NSArray arrayWithArray:achievements];// 返回的是你的所有成就ID.
			for (GKAchievement *tempAchievement in tempArray) 
			{
				[achievementDictionary setObject:tempAchievement forKey:tempAchievement.identifier];
				CCLOG(@"    completed:%d",tempAchievement.completed);
				CCLOG(@"    hidden:%d",tempAchievement.hidden);
				CCLOG(@"    lastReportedDate:%@",tempAchievement.lastReportedDate);
				CCLOG(@"    percentComplete:%f",tempAchievement.percentComplete);
				CCLOG(@"    identifier:%@",tempAchievement.identifier);
			}
		}
	}];
}

- (GKAchievement*)getAchievementForIdentifier:(NSString*)identifier
{
	NSMutableDictionary *achievementDictionary = [[NSMutableDictionary alloc] init];
	GKAchievement *achievement = [achievementDictionary objectForKey:identifier];
	if (achievement == nil)
	{
		achievement = [[[GKAchievement alloc] initWithIdentifier:identifier] autorelease];
		[achievementDictionary setObject:achievement forKey:achievement.identifier];
	}
	return [[achievement retain] autorelease];
}

- (NSArray*)retrieveAchievmentMetadata
{
	//读取成就的描述
	[GKAchievementDescription loadAchievementDescriptionsWithCompletionHandler:^(NSArray *descriptions, NSError *error) 
	{
		if (error != nil)
		{
			// process the errors
			CCLOG(@"读取成就说明出错");
		}
		if (descriptions != nil)
		{
			// use the achievement descriptions.
			for (GKAchievementDescription *achDescription in descriptions) 
			{
				CCLOG(@"1..identifier..%@",achDescription.identifier);
				CCLOG(@"2..achievedDescription..%@",achDescription.achievedDescription);
				CCLOG(@"3..title..%@",achDescription.title);
				CCLOG(@"4..unachievedDescription..%@",achDescription.unachievedDescription);
				CCLOG(@"5............%@",achDescription.image);
				
				//获取成就图片,如果成就未解锁,返回一个大文号
				/*
				[achDescription loadImageWithCompletionHandler:^(UIImage *image, NSError *error) 
				{
					if (error == nil)
					{
						// use the loaded image. The image property is also populated with the same image.
						CCLOG(@"成功取得成就的图片");
						UIImage *aImage = image;
						UIImageView *aView = [[UIImageView alloc] initWithImage:aImage];
						aView.frame = CGRectMake(50, 50, 200, 200);
						aView.backgroundColor = [UIColor clearColor];
						[[[CCDirector sharedDirector] openGLView] addSubview:aView];
					}
					else 
					{
						CCLOG(@"获得成就图片失败");
					}
				}];
				*/
			}
		 }
	}];
	return nil;
}

- (void)dealloc
{
	[super dealloc];
	[instance release];
}

@end
