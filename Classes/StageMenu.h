//
//  ChoiceMenu.h
//  tweejump
//
//  Created by inblue-piepie on 10-7-23.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameStory.h"
#import "MainScene.h"
#import "GameScene.h"
#import "SimpleAudioEngine.h"
#define MAX_MAP 9
typedef struct
{
	CCSprite *p_map_state;
	BOOL islock;
	BOOL choosen;

	
}MapInfo;

@interface StageMenu : CCLayer
{
	int m_state;//关卡控制
	BOOL firstTime;
	BOOL m_endless;
	MapInfo map_state[MAX_MAP];
	int m_index;
	BOOL toRight;
	
	CCSprite *p_backSprite;
	NSMutableArray *p_backNum;
	int m_callbackCount_back;
	int m_mapCurrentIndex_back;
	CCSpeed *p_backSpeed;
	CCNode *p_backmapNode;
	
	CCSprite *p_play_icon;
	CCSprite *p_bg;
	
	
	CCMenuItem *p_arrowL;
	CCMenuItem *p_arrowR;
	CCMenuItem *p_play;
	CCMenuItem *p_unplay;
	CCMenuItem *p_backto;
	
	
	CCSprite *p_bg_front;
	CCSprite *p_state[9];
	CCSprite *p_mapLock[3];
	CCSprite *p_map_lock;
	CCSprite *p_small_lock;
	CCSprite *p_small_open;
}

+(id) scene;
-(void)initSound;
- (id)initWithdata:(BOOL)endless; 
- (void)initItem;
- (void)judge:(int)m_tag;
- (void)showMap;
- (void)initMap;
@end
