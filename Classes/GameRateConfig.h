//
//  GameRateConfig.h
//  All Game
//
//  Created by o0402 on 11-5-25.
//  Copyright 2011 xiaoxin All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"

@interface GameRateConfig : NSObject<UIAlertViewDelegate> 
{
	NSString* m_pStrAppID;
}

@property(nonatomic,assign) NSString* m_pStrAppID;

// 单链实例
+(id) shareGameRateInstance;

// 设置App Story应用专属ID
-(void) initGameRateWithAppID:(NSString*)appID;

@end
