//
//  GameDataDefined.h
//  LoveAdventure
//
//  Created by Huang Xinping on 7/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//
#ifndef __GAME_DATA_DEFINED__H__
#define __GAME_DATA_DEFINED__H__
//
#define isIphoneRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)]?CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size):NO)
#define isPad (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
//
//#ifdef ccp
//#undef ccp
//#endif
//
//#ifdef ccr
//#undef ccr
//#endif
//
//static CGPoint ccpoint(float x,float y)
//{
//	if (isPad) {
//		return CGPointMake(224 + x, 272 + y);
//	}else {
//		return CGPointMake(x, y);
//	}
//}
//
//#define ccp(__X__,__Y__) ccpoint(__X__,__Y__)
//

static CGPoint convertPoint(CGPoint point) 
{    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) 
	{
        return ccp(64 + point.x*2, 32 + point.y*2);
    } 
	else 
	{
        return point;
    }    
}

#endif