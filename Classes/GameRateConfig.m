//
//  GameRateConfig.m
//  All Game
//
//  Created by o0402 on 11-5-25.
//  Copyright 2011 xiaoxin All rights reserved.
//

#import "GameRateConfig.h"

static GameRateConfig* instance = nil; 
@implementation GameRateConfig
@synthesize m_pStrAppID;

+ (id)shareGameRateInstance
{
	if (nil == instance) 
	{
		GameRateConfig* shareInstance = [[GameRateConfig alloc] init];
		instance = shareInstance;
	}
	return instance;
}

- (id) init
{
	if ((self = [super init])) 
	{
		m_pStrAppID = nil;
	}
	return self;
}

-(void) initGameRateWithAppID:(NSString*)appID
{
	m_pStrAppID = [NSString stringWithFormat:@"%@",appID];
	CCLOG(@"%@",m_pStrAppID);
	
	BOOL bRateGame = [[NSUserDefaults standardUserDefaults] boolForKey:@"OnceRateGame"];
	if (!bRateGame) 
	{
		UIAlertView* pAlert = [[[UIAlertView alloc] init] autorelease];
		pAlert.title = @"Enjoying PopoJump?";
		pAlert.message = @"If so,please rate this game on the\nApp Store with 5 stars so we can\nkeep the free updates coming.";
		pAlert.delegate = self;
		[pAlert addButtonWithTitle:@"Yes,rate it!"];
		[pAlert addButtonWithTitle:@"Remind me later"];
		[pAlert addButtonWithTitle:@"Don't ask again"];
		pAlert.transform = CGAffineTransformTranslate(pAlert.transform,0.0,0.0);
		[pAlert show];
	}
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex 
{
	if(buttonIndex == 0) 
	{
		[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"OnceRateGame"];
		NSString* rateURL = [NSString stringWithFormat:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8",m_pStrAppID]; 
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:rateURL]];
	}
	else if(buttonIndex == 1) 
	{
		[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"OnceRateGame"];
	}
	else if(buttonIndex == 2) 
	{
		[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"OnceRateGame"];
	}
}

- (void)dealloc
{
	[super dealloc];
	[instance release];
}

@end
