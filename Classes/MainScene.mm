//
//  MainScene.m
//  FatJumper
//
//  Created by in-blue  on 10-10-11.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MainScene.h"
#import "GameDataDefined.h"

@implementation MainScene
#pragma mark -
+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MainScene *layer = [MainScene node];
	
	// add layer as a child to scene
	[scene addChild: layer];

	// return the scene
	return scene;
}
#pragma mark -
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init] )) 
	{
		firstTime = [[NSUserDefaults standardUserDefaults] boolForKey:@"onetimes"];
		if (firstTime == NO)
		{
			int player = 0;
			firstTime = YES;
			[self initWithPlayer:player];	
			[[NSUserDefaults standardUserDefaults] setBool:firstTime forKey:@"onetimes"];

		}
		else
		{
			int player = [[NSUserDefaults standardUserDefaults] integerForKey: @"playerKey"];
			[self initWithPlayer:player];	
		}

		
	
		self.isTouchEnabled = YES;
		
		m_pTipOpenFaintBack  = nil;
		m_pTipBackGround = nil;
		m_pTipInfo = nil;
		m_pTipGameCenterBack = nil;
		m_pBtnGCLeaderBoards = nil;
		m_pBtnGCAchievement = nil;
		m_pBtnOFLeaderBoards = nil;
		m_pBtnOFAchievement = nil;
		m_bIsShowGCAndOF = false;
	}
	return self;
}


#pragma mark -
- (id)initWithPlayer:(int)player
{
	if (isPad)
	{
		CCSprite *haha = [CCSprite spriteWithFile:@"123456.png"];
		[self addChild:haha z:20];
		[haha setPosition:ccp(384,512)];
	}
	
	m_player = player;
	m_x1 = 0;
	m_x2 = 0;
	p_loading_BG = [CCSprite spriteWithFile:@"logo_loading_BG.png"];
	[p_loading_BG setPosition:convertPoint(ccp(160,240))];
	[self addChild:p_loading_BG z:2];
	
	p_loading_teeth = [CCSprite spriteWithFile:@"logo_loading_teeth.png"];
	[p_loading_teeth setPosition:convertPoint(ccp(65,148))];
	[p_loading_teeth setTextureRect:CGRectMake(0,0,24,62)];
	if (isPad)
		[p_loading_teeth setTextureRect:CGRectMake(0,0,24*2,62*2)];
		
	[self addChild:p_loading_teeth z:3];

	[self initSound];
	p_loading_mouth = [CCSprite spriteWithFile:@"logo_loading_mouth.png"];
	[p_loading_mouth setPosition:convertPoint(ccp(160.5,166.5))];
	[self addChild:p_loading_mouth z:4];
	
	[self schedule:@selector(update:)];
	
	
	
	return self;
}
#pragma mark -
- (void)InitBackground
{
	
	p_backmapNode = [CCNode node];
	[self addChild:p_backmapNode z:1];
	
	p_backNum = [[NSMutableArray alloc] initWithCapacity:10];

	for (int i=0;i<6;i++)
	{
		p_backSprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"stage2_scense_bg_%02d.png",i+1]];
		[p_backSprite setPosition:ccp(160,479*i+240)];
		if (isPad) 
			[p_backSprite setPosition:convertPoint(ccp(160,479*i+240))];	
		[p_backmapNode addChild:p_backSprite z:i-6];
		[p_backNum addObject:p_backSprite];
		[p_backSprite.texture setAliasTexParameters];
	}
	
	id backgoUp = nil;
	if (isPad) 
	{
		backgoUp = [CCRepeatForever actionWithAction:
					[CCSequence actions:
					 [CCMoveBy actionWithDuration:240 position:ccp(0,-479*2)],
					 [CCCallFunc actionWithTarget:self selector:@selector(backCallback)],
					 nil]];		
	}
	else 
	{
		backgoUp = [CCRepeatForever actionWithAction:
					[CCSequence actions:
					 [CCMoveBy actionWithDuration:120 position:ccp(0,-479)],
					 [CCCallFunc actionWithTarget:self selector:@selector(backCallback)],
					 nil]];
	}

	p_backSpeed = [CCSpeed actionWithAction:backgoUp speed:1];
	[p_backmapNode runAction:p_backSpeed];
	p_backSpeed.speed = 20;

}
#pragma mark -
- (void)backCallback
{	
	m_callbackCount_back++;

	CCSprite *tempMap = [p_backNum objectAtIndex:m_mapCurrentIndex_back%6];
	if (isPad) 
		tempMap.position = ccp(tempMap.position.x,tempMap.position.y + 956*6);
	else 
		tempMap.position = ccp(tempMap.position.x,tempMap.position.y + 478*6);
	m_mapCurrentIndex_back++;
	[tempMap.texture setAliasTexParameters];
	
}
#pragma mark -

- (void)InitItem
{
	CGSize winsize = [[CCDirector sharedDirector] winSize];
	
	p_cocpyright = [CCSprite spriteWithFile:@"copyright_bar.png"];
	[self addChild:p_cocpyright z:4];
	[p_cocpyright setPosition:convertPoint(ccp(160,13))];
	p_cocpyright.visible = NO;
	
	p_title = [CCSprite spriteWithFile:@"title_bar.png"];
	[self addChild:p_title z:1];
	[p_title setPosition:convertPoint(ccp(160,480))];
	p_title.visible = NO;	
	
	

	
	
	p_mainmenuback = [CCSprite spriteWithFile:@"mainmenu_bg_back.png"];
	[self addChild:p_mainmenuback z:3];
	[p_mainmenuback setPosition:convertPoint(ccp(160,-100))];
	p_mainmenuback.visible = NO;
	
	p_mainmenufront = [CCSprite spriteWithFile:@"mainmenu_bg_front.png"];
	[p_mainmenuback addChild:p_mainmenufront z:3];
	[p_mainmenufront setPosition:ccp(160,120)];
	if (isPad)
	{
		[p_mainmenufront setPosition:convertPoint(ccp(124,100))];
	}


	
	
	p_mainmenufront.visible = NO;
	
	
	
	start = [CCMenuItemImage itemFromNormalImage:@"story_button.png"
								   selectedImage:@"story_button_down.png"
										  target:self
										selector:@selector(startGame:)];
	unlimted = [CCMenuItemImage itemFromNormalImage:@"unlimited_button.png"
								  selectedImage:@"unlimited_button_down.png"
										 target:self
									   selector:@selector(startunlimiteGame:)];
	
	start.position = convertPoint(ccp(25,-57));
	[start setIsEnabled:NO];
	
	unlimted.position = convertPoint(ccp(25,-119));
	[unlimted setIsEnabled:NO];

	
	
	open = [CCMenuItemImage itemFromNormalImage:@"leadboard_button.png"
								  selectedImage:@"leadboard_button_down.png"
										 target:self
									   selector:@selector(showHighscore:)];
	open.position = convertPoint(ccp(25,-179));
	[open setIsEnabled:NO];
	
	label = [CCMenuItemImage itemFromNormalImage:@"mainmenu_bg_label.png"
								  selectedImage:@"mainmenu_bg_label.png"
										 target:self
									   selector:@selector(choosePlayer:)];
	label.anchorPoint = ccp(0.08,0.88);
	label.position = ccp(37,20);
	if (isPad)
	label.position = ccp(160,144);	

	[label setIsEnabled:NO];
	
	CCMenu *menu = [CCMenu menuWithItems:start,unlimted,open,label,nil];
	if (isPad)
	{	
		menu.position = ccp(250,435);	
	}	
	[p_mainmenuback addChild:menu z:4];	
	
	p_title_anim = [CCSprite spriteWithFile:@"title_01.png"];
	[self addChild:p_title_anim z:2];
	[p_title_anim setPosition:convertPoint(ccp(160,365))];
	p_title_anim.visible = NO;
	CCAnimation *p_Animation = [CCAnimation animationWithName:@"Animation" delay:0.1];
	for (int i=1; i<14; i++)
	{
		[p_Animation addFrameWithFilename:[NSString stringWithFormat:@"title_%02d.png", i]];
	}
	id action = [CCAnimate actionWithAnimation:p_Animation restoreOriginalFrame:NO];
	[p_title_anim runAction:[CCRepeatForever actionWithAction:action]]; 
	
	switch (m_player)
	{
		case 0:
			p_player = [CCSprite spriteWithFile:@"mainmenu_bg_photo1.png"];
			if (isPad)
			{
				[p_player setPosition:ccp(310,400)];
			}
			else
			{
				[p_player setPosition:ccp(150,170)];
 			}

			[p_mainmenuback addChild:p_player z:1];
			break;
		case 1:
			p_player = [CCSprite spriteWithFile:@"mainmenu_bg_photo2.png"];
			if (isPad)
			{
				[p_player setPosition:ccp(310,400)];
			}
			else
			{
				[p_player setPosition:ccp(150,170)];
 			}
			
			[p_mainmenuback addChild:p_player z:1];
			break;
		case 2:
			p_player = [CCSprite spriteWithFile:@"mainmenu_bg_photo3.png"];
			if (isPad)
			{
				[p_player setPosition:ccp(310,400)];
			}
			else
			{
				[p_player setPosition:ccp(150,170)];
 			}			
			[p_mainmenuback addChild:p_player z:1];
			break;
		case 3:
			p_player = [CCSprite spriteWithFile:@"mainmenu_bg_photo4.png"];
			if (isPad)
			{
				[p_player setPosition:ccp(310,400)];
			}
			else
			{
				[p_player setPosition:ccp(150,170)];
 			}
			
			[p_mainmenuback addChild:p_player z:1];
			break;	
		default:
			break;
	}
//	

	m_pBtnGCLeaderBoards = [CCMenuItemImage itemFromNormalImage:@"Leaderboard_button.png"
												  selectedImage:@"Leaderboard_button_down.png"
														 target:self
													   selector:@selector(lauchedGCLeaderBoards:)];
	m_pBtnGCLeaderBoards.position = convertPoint(ccp(20,138+360));
	
	m_pBtnGCAchievement = [CCMenuItemImage itemFromNormalImage:@"Achievement_button.png"
												selectedImage:@"Achievement_button_down.png"
													   target:self
													 selector:@selector(lauchedGCAchievement:)];
	m_pBtnGCAchievement.position = convertPoint(ccp(20,88+360));
	
	m_pBtnOFLeaderBoards = [CCMenuItemImage itemFromNormalImage:@"Leaderboard_button.png"
												  selectedImage:@"Leaderboard_button_down.png"
														 target:self
													   selector:@selector(lauchedOFLeaderBoards:)];
	m_pBtnOFLeaderBoards.position = convertPoint(ccp(20,-23+360));
	
	m_pBtnOFAchievement = [CCMenuItemImage itemFromNormalImage:@"Achievement_button.png"
												 selectedImage:@"Achievement_button_down.png"
														target:self
													  selector:@selector(lauchedOFAchievement:)];
	m_pBtnOFAchievement.position = convertPoint(ccp(20,-73+360));
	
	CCMenu* pGCMenus = [CCMenu menuWithItems:m_pBtnGCLeaderBoards,m_pBtnGCAchievement,m_pBtnOFLeaderBoards,m_pBtnOFAchievement,nil];
	if (isPad) 
	{
		pGCMenus.position = ccp(384-60,512-35);
	}
	[self addChild:pGCMenus z:22];
}

-(void) lauchedGCLeaderBoards:(id)sender
{
	[[GameCenterConfig shareGameCenterInstance] showLeaderboard];
}

-(void) lauchedGCAchievement:(id)sender
{
	[[GameCenterConfig shareGameCenterInstance] showAchievement];
}

-(void) lauchedOFLeaderBoards:(id)sender
{
	[OpenFeint launchDashboard];
}

-(void) lauchedOFAchievement:(id)sender
{
	[OpenFeint launchDashboard];
}

#pragma mark -
- (void)actionDone1:(id)sender
{ 
	p_title_anim.visible = YES;
	[[SimpleAudioEngine sharedEngine] playEffect:@"photo.aif"];
	[p_mainmenuback runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.1 position:convertPoint(ccp(160,138))],[CCMoveBy actionWithDuration:0.3 position:ccp(0,-20)],[CCMoveBy actionWithDuration:0.3 position:ccp(0,10)],[CCCallFuncN actionWithTarget:self selector:@selector(actionDone2:)],nil]];	
	
}
#pragma mark -
- (void)actionDone2:(id)sender
{
	[[SimpleAudioEngine sharedEngine] playEffect:@"photo.aif"];
	[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"Mainscene.aif" loop:YES]; 	

	[start setIsEnabled:YES];
	[unlimted setIsEnabled:YES];
	[open setIsEnabled:YES];
	[label setIsEnabled:YES];
	[p_player runAction:[CCSequence actions:[CCMoveBy actionWithDuration:0.05 position:ccp(0,80)],[CCMoveBy actionWithDuration:0.2 position:ccp(0,-8)],nil]];	
	[label runAction:[CCSequence actions:[CCMoveBy actionWithDuration:0.1 position:ccp(3,84)],[CCMoveBy actionWithDuration:0.2 position:ccp(0,-8)],nil]];
	id action = [CCRotateBy actionWithDuration:0.2 angle: -5];
	id actionBy = [CCRotateBy actionWithDuration:0.2  angle: 5];
	id actionByBack = [CCRotateBy actionWithDuration:0.2  angle: -2];
	id action2 = [CCRotateBy actionWithDuration:0.2  angle: 2];
	[label runAction:[CCRepeatForever actionWithAction:[CCSequence actions:action,actionBy, actionByBack,action2, [CCDelayTime actionWithDuration:1.27],nil]]];
	
}
#pragma mark -
- (void)update: (ccTime)t 
{
	m_x1 += 24;
	m_x2 += 12;

	int nEndPosition = 192;
	if (isPad) {
		nEndPosition = 192*2;
		m_x1 += 24;
	}
	if (m_x1 <= nEndPosition)
	{
		[self Loading];
		[p_loading_teeth setTextureRect: CGRectMake(0,0,m_x1+24,62)];
		if (isPad) {
			[p_loading_teeth setTextureRect: CGRectMake(0,0,m_x1+24*2,62*2)];
		}
		[p_loading_teeth setPosition:convertPoint(ccp(64+m_x2,148))];
	}
	else
	{
		[self Start];
	}	
}
#pragma mark -
- (void)Loading
{
	if (isPad) 
	{
		switch (m_x1)
		{
			case 0:
			case 72*2:
			{
				
				[self InitBackground];
			}
				break;
			case 192*2:
			{
				[self InitItem];
			}
				break;
			default:
				break;
		}
		return;
	}
	
	
	switch (m_x1)
	{
		case 0:
		case 72:
		{
			[self InitBackground];
		}
			break;
		case 192:
		{
			[self InitItem];
		}
			break;
		default:
			break;
	}
}
#pragma mark -
- (void)Start
{
	[self unschedule:@selector(update:)];
	[self removeChild:p_loading_mouth cleanup:YES];
	[self removeChild:p_loading_BG cleanup:YES];
	[self removeChild:p_loading_teeth cleanup:YES];
	p_title.visible = YES;
	p_mainmenufront.visible = YES;
	p_mainmenuback.visible = YES;
	p_title_anim.visible = NO;
	p_cocpyright.visible = YES;
	
	[p_title runAction:[CCSequence actions:[CCMoveTo actionWithDuration:0.1 position:convertPoint(ccp(160,370))],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-5)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,5)],[CCMoveBy actionWithDuration:0.1 position:ccp(0,-10)],[CCCallFuncN actionWithTarget:self selector:@selector(actionDone1:)],nil]];	
	[[SimpleAudioEngine sharedEngine] playEffect:@"Mainscenelogo.aif"]; 
}

#pragma mark -
- (void)choosePlayer:(id)sender;
{
	[[SimpleAudioEngine sharedEngine] playEffect:@"ChangeplayerTip.aif"];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Replacescene.aif"]; 
	
	[self unscheduleAllSelectors];
	[self removeAllChildrenWithCleanup:YES];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[ChooseScene scene] withColor:ccWHITE]];
	
}
#pragma mark -
- (void)startGame:(id)sender;
{
	[[SimpleAudioEngine sharedEngine] playEffect:@"Confirm.aif"];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Replacescene.aif"];
	endless = NO;
	[[NSUserDefaults standardUserDefaults] setBool:endless forKey:@"GameEndless"];
	[self removeAllChildrenWithCleanup:YES];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[StageMenu scene] withColor:ccWHITE]];
}
#pragma mark -
- (void)startunlimiteGame:(id)sender;
{
	[[SimpleAudioEngine sharedEngine] playEffect:@"Confirm.aif"];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Replacescene.aif"];
	
	endless = YES;
	[[NSUserDefaults standardUserDefaults] setBool:endless forKey:@"GameEndless"];
	[self removeAllChildrenWithCleanup:YES];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[StageMenuUn scene] withColor:ccWHITE]];
	
	
}

-(void) leaderboardCallBack:(id) sender
{
	m_bIsShowGCAndOF = true;
}

#pragma mark -
- (void)showHighscore:(id)sender;
{
	[[SimpleAudioEngine sharedEngine] playEffect:@"Confirm.aif"];
	[[SimpleAudioEngine sharedEngine] playEffect:@"Replacescene.aif"];
	
	//[OpenFeint launchDashboard];
	
	m_bIsShowGCAndOF = false;
	
	[start setIsEnabled:NO];
	[unlimted setIsEnabled:NO];
	[open setIsEnabled:NO];
	[label setIsEnabled:NO];
	
	m_pTipBackGround = [CCSprite spriteWithFile:@"choose_board_black.png"];
	m_pTipBackGround.position = convertPoint(ccp(160,240));
	[self addChild:m_pTipBackGround z:10];
	
	m_pTipInfo = [CCSprite spriteWithFile:@"MainScene_TIP.png"];
	m_pTipInfo.position = convertPoint(ccp(160,90));
	[self addChild:m_pTipInfo z:18];
	id action = [CCRepeatForever actionWithAction:[CCBlink actionWithDuration:1 blinks:1]];
	[m_pTipInfo runAction:action];
	
	id moveBy = nil;
	if (!isPad) 
	{
		moveBy = [CCMoveBy actionWithDuration:0.5f position:ccp(0,-340)];
	}
	else 
	{
		moveBy = [CCMoveBy actionWithDuration:0.5f position:ccp(0,-340*2)];	
	}
	
	id moveEaseBackInOut = [CCEaseBackInOut actionWithAction:[[moveBy copy] autorelease]];
	id moveBySmall = nil;
	if (!isPad) 
	{
		moveBySmall = [CCMoveBy actionWithDuration:0.2f position:ccp(0,-20)];
	}
	else 
	{
		moveBySmall = [CCMoveBy actionWithDuration:0.2f position:ccp(0,-20*2)];
	}
	
	id moveEaseBackInOutSmall = [CCEaseBackInOut actionWithAction:[[moveBySmall copy] autorelease]];
	
	m_pTipGameCenterBack = [CCSprite spriteWithFile:@"MainScene_GC.png"];
	m_pTipGameCenterBack.position = convertPoint(ccp(160,720));
	[self addChild:m_pTipGameCenterBack z: 21];
	[m_pTipGameCenterBack runAction:[CCSequence actions:
									 [[moveEaseBackInOut copy] autorelease],[[moveEaseBackInOutSmall copy] autorelease],nil]];
	
	m_pTipOpenFaintBack = [CCSprite spriteWithFile:@"MainScene_OF.png"];
	m_pTipOpenFaintBack.position = convertPoint(ccp(160,560));
	[self addChild:m_pTipOpenFaintBack z: 21];
	[m_pTipOpenFaintBack runAction:[CCSequence actions:
									[[moveEaseBackInOut copy] autorelease],[[moveEaseBackInOutSmall copy] autorelease],nil]];
	
	[m_pBtnGCLeaderBoards runAction:[CCSequence actions:
									 [[moveEaseBackInOut copy] autorelease],[[moveEaseBackInOutSmall copy] autorelease],nil]];
	[m_pBtnGCAchievement runAction:[CCSequence actions:
									[[moveEaseBackInOut copy] autorelease],[[moveEaseBackInOutSmall copy] autorelease],nil]];
	[m_pBtnOFLeaderBoards runAction:[CCSequence actions:
									 [[moveEaseBackInOut copy] autorelease],[[moveEaseBackInOutSmall copy] autorelease],nil]];
	[m_pBtnOFAchievement runAction:[CCSequence actions:
									[[moveEaseBackInOut copy] autorelease],[[moveEaseBackInOutSmall copy] autorelease],
									[CCCallFuncN actionWithTarget:self selector:@selector(leaderboardCallBack:)],nil]];
}

-(void)initSound
{
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"ChangeplayerTip.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Confirm.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Mainscenelogo.aif"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"Replacescene.aif"];
	[[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"Mainscene.aif"]; 	
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"photo.aif"];
	
}

#pragma mark -
- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (m_bIsShowGCAndOF && m_pTipOpenFaintBack != nil && m_pTipBackGround != nil && m_pTipInfo != nil && m_pTipGameCenterBack != nil) 
	{
		m_pTipOpenFaintBack.visible = NO;
		m_pTipBackGround.visible = NO;
		m_pTipInfo.visible = NO;
		m_pTipGameCenterBack.visible = NO;
		
		[self removeChild:m_pTipOpenFaintBack cleanup:YES];
		[self removeChild:m_pTipBackGround cleanup:YES];
		[self removeChild:m_pTipInfo cleanup:YES];
		[self removeChild:m_pTipGameCenterBack cleanup:YES];
		
		[start setIsEnabled:YES];
		[unlimted setIsEnabled:YES];
		[open setIsEnabled:YES];
		[label setIsEnabled:YES];
		
		m_pBtnGCLeaderBoards.position = convertPoint(ccp(20,138+360));
		m_pBtnGCAchievement.position = convertPoint(ccp(20,88+360));
		m_pBtnOFLeaderBoards.position = convertPoint(ccp(20,-23+360));
		m_pBtnOFAchievement.position = convertPoint(ccp(20,-73+360));
		
		m_bIsShowGCAndOF = false;
	}
}

#pragma mark -

- (void)dealloc
{
	[p_backNum release];
	p_backNum = nil;
	[[CCDirector sharedDirector] purgeCachedData];
	[[CCTextureCache sharedTextureCache] removeAllTextures];
	[super dealloc];
	
}

@end
